<?php

namespace App\Http\Controller\DealTabs\Fondy;

use App\Infrastructure\Billing\Fondy\Sdk\FondySdkFactory;
use App\Infrastructure\Model\BillingCycle;
use App\Infrastructure\Service\DealTabsPrices;
use App\Infrastructure\Service\Logger;
use Cloudipsp\Checkout;
use Cloudipsp\Subscription;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route("/{_locale}/dealtabs/fondy/redirect-to-checkout", methods: ['POST'])]
class RedirectToCheckoutAction extends AbstractController
{
    public function __construct(
        private DealTabsPrices $dealTabsPrices,
        private TranslatorInterface $translator,
        private UrlGeneratorInterface $urlGenerator,
        private Logger $logger
    ) {}

    public function __invoke(Request $request)
    {
        FondySdkFactory::setCredentials();

        $data = $request->request->all();
        $billingCycle = (int) $data['billing_cycle'];
        $amountInCents = (int) ($this->dealTabsPrices->getFromBillingCycle($billingCycle) * 100);

        if (empty($data['email'])) {
            throw new BadRequestHttpException('Customer email is missing');
        }

        $checkoutData = [
            'order_desc' => $this->translator->trans('Premium subscription on DealTabs for') . ' ' . $data['email'],
            'currency' => 'USD',
            'amount' => $amountInCents,
            'response_url' => $_ENV['DEALTABS_UI_HOST'] . '/membership?result=order_processed&timeout=5000',
            'server_callback_url' => $_ENV['APP_HOST'] . $this->urlGenerator->generate('dealtabs_fondy_payment_processed_webhook'),
            'sender_email' => $data['email'],
            'lang' => $request->getLocale(),
            'product_id' => "dealtabs_premium_{$data['billing_cycle']}",
            'lifetime' => 36000,
            'merchant_data' => [
                'email' => $data['email'],
                'billing_cycle' => $billingCycle
            ]
        ];

        $recurring = false;
        if (in_array($billingCycle, [BillingCycle::RECURRING_MONTHLY, BillingCycle::RECURRING_YEARLY], true)) {
            $checkoutData['recurring_data'] = [
                'start_time' => (new \DateTimeImmutable())->format('Y-m-d'),
                'amount' => $amountInCents,
                'period' => $billingCycle === BillingCycle::RECURRING_MONTHLY ? 'day' : 'month',
                'every' => $billingCycle === BillingCycle::RECURRING_MONTHLY ? 30 : 12,
                'state' => 'y',
                'readonly' => 'y'
            ];

            $recurring = true;
        }

        $checkout = $recurring ? Subscription::url($checkoutData)->getData() : Checkout::url($checkoutData)->getData();

        return $this->redirect($checkout['checkout_url']);
    }
}