<?php

namespace App\Http\Controller\DealTabs\Fondy;

use App\Application\Command\DealTabs\Billing\Fondy\InvalidateAllPaymentsCommand;
use App\Application\Command\DealTabs\Billing\Fondy\StopAllSubscriptionsCommand;
use App\Application\Command\DealTabs\User\MakeUserPremiumCommand;
use App\Application\Event\DealTabs\Billing\PaymentProcessedEvent;
use App\Database\Domain\Entity\DealTabs\User\User;
use App\Database\Domain\Entity\Main\Billing\Fondy\FondyProcessedPayment;
use App\Database\Domain\Entity\Main\Billing\Fondy\FondySubscription;
use App\Database\Domain\Repository\DealTabs\UserRepository as DealTabsUserRepository;
use App\Database\Domain\Repository\Main\Fondy\FondySubscriptionRepository;
use App\Infrastructure\Billing\Fondy\FondyCredentials;
use App\Infrastructure\Billing\Fondy\Sdk\FondySignature;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use App\Infrastructure\Messenger\EventBus\EventBusInterface;
use App\Infrastructure\Model\App;
use App\Infrastructure\Model\BillingCycle;
use App\Infrastructure\Service\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/dealtabs/fondy/payment-processed", name: 'dealtabs_fondy_payment_processed_webhook', methods: ['POST'])]
class PaymentProcessedAction extends AbstractController
{
    public function __construct(
        private Logger $logger,
        private DealTabsUserRepository $userRepository,
        private EventBusInterface $eventBus,
        private CommandBusInterface $commandBus,
        private FondySubscriptionRepository $fondySubscriptionRepository
    ) {
    }

    public function __invoke(Request $request)
    {
        $requestData = json_decode($request->getContent(), true);
        $this->logger->info('Fondy payment processed request', $requestData);

        FondySignature::merchantId(FondyCredentials::getEnvMerchantId());
        FondySignature::paymentKey(FondyCredentials::getEnvPaymentKey());

        if (!FondySignature::check($requestData)) {
            $this->logger->error('Fondy signature check failed', $requestData);

            return $this->json(['message' => 'Signature check failed'], 403);
        }

        $em = $this->getDoctrine()->getManager();

        $decodedData = json_decode(base64_decode($requestData['data']), true)['order'];
        $merchantData = json_decode($decodedData['merchant_data'], true);

        if (!in_array($merchantData['billing_cycle'], BillingCycle::all())) {
            $this->logger->error('Invalid billing cycle', $decodedData);
            $this->json(['message' => 'Invalid billing cycle'], 400);
        }

        if (in_array($merchantData['billing_cycle'], BillingCycle::getOneTimeCycles())) {
            $this->commandBus->execute(
                new StopAllSubscriptionsCommand($merchantData['email'])
            );

            $this->commandBus->execute(
                new InvalidateAllPaymentsCommand($merchantData['email'])
            );

            $validityDate = $merchantData['billing_cycle'] === BillingCycle::ONE_TIME_MONTHLY
                ? new \DateTimeImmutable('now + 30 days')
                : new \DateTimeImmutable('now + 365 days');

            $processedPayment = new FondyProcessedPayment(
                App::DEALTABS,
                $decodedData['order_id'],
                $merchantData['email'],
                $merchantData['billing_cycle'],
                $validityDate
            );

            $em->persist($processedPayment);

            $this->makeDealTabsUserPremium($merchantData['email'], $merchantData['billing_cycle']);
        }

        if (in_array($merchantData['billing_cycle'], BillingCycle::getRecurringCycles())) {
            $expiringDate = $merchantData['billing_cycle'] === BillingCycle::RECURRING_MONTHLY
                ? new \DateTimeImmutable('now + 30 days')
                : new \DateTimeImmutable('now + 365 days');

            /** @var FondySubscription $subscription */
            $subscription = $this->fondySubscriptionRepository->findOneBy(
                [
                    'app' => App::DEALTABS,
                    'customerEmail' => $merchantData['email'],
                    'stopDate' => null,
                    'externalId' => $decodedData['order_id']
                ]
            );

            if (null === $subscription) {
                $this->commandBus->execute(
                    new StopAllSubscriptionsCommand($merchantData['email'])
                );

                $this->commandBus->execute(
                    new InvalidateAllPaymentsCommand($merchantData['email'])
                );

                $subscription = new FondySubscription(
                    App::DEALTABS,
                    $decodedData['order_id'],
                    $merchantData['email'],
                    $merchantData['billing_cycle'],
                    $expiringDate
                );

                $em->persist($subscription);
            } else {
                $subscription->setExpiringDate($expiringDate);
            }

            $this->makeDealTabsUserPremium($merchantData['email'], $merchantData['billing_cycle']);
        }

        $em->flush();

        return $this->json(['message' => 'Payment processed successfully']);
    }

    private function makeDealTabsUserPremium(string $customerEmail, int $billingCycle)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $customerEmail]);

        $this->eventBus->fire(new PaymentProcessedEvent($user));

        $command = new MakeUserPremiumCommand();
        $command->user = $user->getEmail();
        $command->subscriptionExpires = $this->getSubscriptionExpiresFromBillingCycle($billingCycle)->format('c');
        $command->membershipType = 'premium';
        $command->subscriptionType = $this->getSubscriptionTypeFromBillingCycle($billingCycle);

        $this->commandBus->execute($command);
    }

    private function getSubscriptionExpiresFromBillingCycle(int $billingCycle): \DateTimeInterface
    {
        switch ($billingCycle) {
            case BillingCycle::RECURRING_MONTHLY:
            case BillingCycle::ONE_TIME_MONTHLY:
            {
                return new \DateTimeImmutable('now + 30 days');
            }
            case BillingCycle::RECURRING_YEARLY:
            case BillingCycle::ONE_TIME_YEARLY:
            {
                return new \DateTimeImmutable('now + 365 days');
            }
            default:
                throw new \LogicException('Order was created incorrectly');
        }
    }

    private function getSubscriptionTypeFromBillingCycle(int $billingCycle): string
    {
        switch ($billingCycle) {
            case BillingCycle::ONE_TIME_MONTHLY:
            {
                return 'fondy_one_time_month';
            }
            case BillingCycle::ONE_TIME_YEARLY:
            {
                return 'fondy_one_time_year';
            }
            case BillingCycle::RECURRING_MONTHLY:
            {
                return 'fondy_recurring_monthly';
            }
            case BillingCycle::RECURRING_YEARLY:
            {
                return 'fondy_recurring_yearly';
            }
            default:
                throw new \LogicException('Order was created incorrectly');
        }
    }
}