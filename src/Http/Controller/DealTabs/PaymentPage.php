<?php

namespace App\Http\Controller\DealTabs;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

#[Route("/{_locale}/dealtabs/payment", methods: ['GET'])]
class PaymentPage extends AbstractController
{
    public function __invoke(UrlGeneratorInterface $urlGenerator)
    {
        return $this->render('dealtabs/pages/payment_page.html.twig');
    }
}