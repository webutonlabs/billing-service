<?php

namespace App\Http\Controller\DealTabs\Liqpay;

use App\Application\Command\DealTabs\Billing\Liqpay\StopAllSubscriptionsCommand as LiqpayStopAllSubscriptionsCommand;
use App\Application\Command\DealTabs\User\MakeUserPremiumCommand;
use App\Application\Event\DealTabs\Billing\PaymentProcessedEvent;
use App\Database\Domain\Entity\DealTabs\User\User;
use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder;
use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpaySubscription;
use App\Database\Domain\Repository\DealTabs\UserRepository;
use App\Database\Domain\Repository\Main\Liqpay\LiqpayOrderRepository;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Billing\Liqpay\LiqpayCredentials;
use App\Infrastructure\Billing\Liqpay\LiqpaySecurity;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use App\Infrastructure\Messenger\EventBus\EventBusInterface;
use App\Infrastructure\Model\App;
use App\Infrastructure\Model\BillingCycle;
use App\Infrastructure\Service\Logger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

#[Route("/dealtabs/liqpay/order-processed", name: 'liqpay_order_processed', methods: ['POST'])]
class OrderProcessedAction extends AbstractController
{
    public function __construct(
        private CommandBusInterface $commandBus,
        private LiqpayCredentials $credentials,
        private LiqpayOrderRepository $liqpayOrderRepository,
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository,
        private UserRepository $userRepository,
        private EventBusInterface $eventBus,
        private Logger $logger
    ) {}

    public function __invoke(Request $request)
    {
        $signature = $request->request->get('signature');
        $data = $request->request->get('data');

        if (!LiqpaySecurity::checkSignature($signature, $data, $this->credentials->getEnvPrivateKey())) {
            throw new AccessDeniedException();
        }

        $data = json_decode(base64_decode($data), true);
        $this->logger->info('Liqpay order processed', $data);

        $em = $this->getDoctrine()->getManager();

        /** @var LiqpayOrder $order */
        $order = $this->liqpayOrderRepository->findOneBy(['externalId' => $data['order_id']]);
        if (null === $order) {
            throw new NotFoundHttpException('Order was not found');
        }

        $order->setStatus(LiqpayOrder::STATUS_PROCESSED);
        $order->setLiqpayPaymentId($data['payment_id']);

        $customerEmail = $order->getExternalData()['customer_email'];
        $subscriptionExpires = $this->getSubscriptionExpiresFromOrder($order);

        if ($order->isRecurring()) {
            /** @var LiqpaySubscription $subscription */
            $subscription = $this->liqpaySubscriptionRepository->findOneBy(
                    [
                        'app' => App::DEALTABS,
                        'customerEmail' => $customerEmail,
                        'order' => $order
                    ]
            );

            if (null === $subscription) {
                $this->commandBus->execute(new LiqpayStopAllSubscriptionsCommand($customerEmail)); // validation previous subscription in case of their existance

                $subscription = new LiqpaySubscription(App::DEALTABS, $order, $customerEmail, $subscriptionExpires);
                $em->persist($subscription);

                $this->makePremium($customerEmail, $subscriptionExpires, $order);
            } else {
                if (!$subscription->isCancelled()) { // recurring payment received
                    $subscription->setExpiringDate($subscriptionExpires);
                    $this->makePremium($customerEmail, $subscriptionExpires, $order);
                }
            }

            $order->setStatus(LiqpayOrder::STATUS_CREATED_SUBSCRIPTION);
        } else {
            $order->setValidityDate($subscriptionExpires);
            $this->makePremium($customerEmail, $subscriptionExpires, $order);
        }

        $em->flush();

        return $this->json([]);
    }

    private function makePremium(string $customerEmail, \DateTimeInterface $subscriptionExpires, LiqpayOrder $order)
    {
        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $customerEmail]);

        $this->eventBus->fire(new PaymentProcessedEvent($user));

        $command = new MakeUserPremiumCommand();
        $command->user = $user->getEmail();
        $command->subscriptionExpires = $subscriptionExpires->format('c');
        $command->membershipType = 'premium';
        $command->subscriptionType = $this->getSubscriptionTypeFromOrder($order);

        $this->commandBus->execute($command);
    }

    private function getSubscriptionExpiresFromOrder(LiqpayOrder $order): \DateTimeInterface
    {
        switch ($order->getBillingCycle()) {
            case BillingCycle::RECURRING_MONTHLY:
            case BillingCycle::ONE_TIME_MONTHLY: {
                return new \DateTimeImmutable('now + 30 days');
            }
            case BillingCycle::RECURRING_YEARLY:
            case BillingCycle::ONE_TIME_YEARLY: {
                return new \DateTimeImmutable('now + 365 days');
            }
            default:
                throw new \LogicException('Order was created incorrectly');
        }
    }

    private function getSubscriptionTypeFromOrder(LiqpayOrder $order): string
    {
        switch ($order->getBillingCycle()) {
            case BillingCycle::ONE_TIME_MONTHLY: {
                return 'liqpay_one_time_month';
            }
            case BillingCycle::ONE_TIME_YEARLY: {
                return 'liqpay_one_time_year';
            }
            case BillingCycle::RECURRING_MONTHLY: {
                return 'liqpay_recurring_monthly';
            }
            case BillingCycle::RECURRING_YEARLY: {
                return 'liqpay_recurring_yearly';
            }
            default:
                throw new \LogicException('Order was created incorrectly');
        }
    }
}