<?php

namespace App\Http\Controller\DealTabs\Liqpay;

use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Billing\Liqpay\Sdk\LiqpaySdkFactory;
use App\Infrastructure\Model\App;
use App\Infrastructure\Model\BillingCycle;
use App\Infrastructure\Service\DealTabsPrices;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

#[Route("/{_locale}/dealtabs/liqpay/create-order", methods: ['POST'])]
class CreateOrderAction extends AbstractController
{
    public function __construct(
        private LiqpaySdkFactory $liqpaySdkFactory,
        private DealTabsPrices $dealTabsPrices,
        private TranslatorInterface $translator,
        private UrlGeneratorInterface $urlGenerator,
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository
    ) {}

    public function __invoke(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        if (empty($data['email'])) {
            throw new BadRequestHttpException('Customer email is missing');
        }

        $order = new LiqpayOrder(
            Uuid::uuid4()->toString(),
            App::DEALTABS,
            $data['billing_cycle'],
            ['customer_email' => $data['email']]
        );

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        $liqpayRequest = [
            'amount' => $this->dealTabsPrices->getFromBillingCycle($order->getBillingCycle()),
            'currency' => 'USD',
            'description' => $this->translator->trans('Premium subscription on DealTabs for') . ' ' . $data['email'],
            'order_id' => $order->getExternalId(),
            'version' => '3',
            'language' => $request->getLocale(),
            'result_url' => $_ENV['DEALTABS_UI_HOST'] . '/membership?result=order_processed',
            'server_url' => $_ENV['APP_HOST'] . $this->urlGenerator->generate('liqpay_order_processed')
        ];

        $liqpayRequest['action'] = $order->isRecurring() ? 'subscribe' : 'pay';
        if ($order->isRecurring()) {
            $liqpayRequest['subscribe'] = '1';
            $liqpayRequest['subscribe_date_start'] = (new \DateTimeImmutable('now'))->format('Y-m-d H:i:s');
            $liqpayRequest['subscribe_periodicity'] =
                $order->getBillingCycle() === BillingCycle::RECURRING_MONTHLY ? 'month' : 'year';
        }

        $sdk = $this->liqpaySdkFactory->getLiqpaySdk();
        list($data, $signature) = $sdk->cnb_form($liqpayRequest);

        return $this->json(['data' => $data, 'signature' => $signature]);
    }
}