<?php

namespace App\Http\Controller\DealTabs;

use App\Database\Domain\Repository\Main\Fondy\FondySubscriptionRepository;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Model\App;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/{_locale}/dealtabs/check-subscription-existence", methods: ['get'])]
class CheckSubscriptionExistenceAction extends AbstractController
{
    public function __construct(
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository,
        private FondySubscriptionRepository $fondySubscriptionRepository
    ) {}

    public function __invoke(Request $request)
    {
        $anySubscriptionExist = false;

        $liqpaySubscription = $this->liqpaySubscriptionRepository->findOneBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $request->query->get('email'),
                'isCancelled' => false
            ]
        );

        if (null !== $liqpaySubscription) {
            $anySubscriptionExist = true;
        }

        $fondySubscription = $this->fondySubscriptionRepository->findOneBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $request->query->get('email'),
                'stopDate' => null
            ]
        );

        if (null !== $fondySubscription) {
            $anySubscriptionExist = true;
        }

        if ($anySubscriptionExist) {
            return $this->json(['message' => 'You must cancel your previous subscription before creating a new one']);
        }

        return $this->json(['message' => 'None subscriptions were found'], 404);
    }
}