<?php

namespace App\Http\Controller\DealTabs;

use App\Application\Command\DealTabs\Billing\Liqpay\CancelSubscriptionCommand as LiqpayCancelSubscriptionCommand;
use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpaySubscription;
use App\Database\Domain\Repository\Main\Fondy\FondySubscriptionRepository;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use App\Infrastructure\Model\App;
use App\Infrastructure\Security\AccessChecker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route("/{_locale}/dealtabs/cancel-subscription", methods: ['PATCH'])]
class CancelSubscriptionAction extends AbstractController
{
    public function __construct(
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository,
        private CommandBusInterface $commandBus,
        private FondySubscriptionRepository $fondySubscriptionRepository
    ) {
    }

    public function __invoke(Request $request)
    {
        AccessChecker::passOrThrow($request);

        $data = json_decode($request->getContent(), true);
        $customerEmail = $data['email'];
        $anySubscriptionExist = false;

        /** @var LiqpaySubscription $liqpaySubscription */
        $liqpaySubscription = $this->liqpaySubscriptionRepository->findOneBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $customerEmail,
                'isCancelled' => false
            ]
        );

        if (null !== $liqpaySubscription) {
            $anySubscriptionExist = true;
            $this->commandBus->execute(new LiqpayCancelSubscriptionCommand($liqpaySubscription->getId()));
        }

        $fondySubscription = $this->fondySubscriptionRepository->findOneBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $customerEmail,
                'stopDate' => null
            ]
        );

        if (null !== $fondySubscription) {
            $anySubscriptionExist = true;

            return $this->json(
                [
                    'resource' => 'fondy',
                    'message' => 'Unfortunately FONDY does not support automatic subscription cancellation. Please contact us via support center.'
                ],
                501
            );
        }

        if (!$anySubscriptionExist) {
            throw new NotFoundHttpException('None subscriptions were found');
        }

        return $this->json([], 204);
    }
}