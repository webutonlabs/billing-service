<?php

namespace App\Application\EventHandler\DealTabs\User;

use App\Application\Event\DealTabs\User\UserCancelledSubscriptionEvent;
use App\Infrastructure\ExternalService\NotificationsClient;
use App\Infrastructure\Messenger\EventBus\EventHandlerInterface;

class UserCancelledSubscriptionEventHandler implements EventHandlerInterface
{
    public function __construct(
        private NotificationsClient $notificationsClient
    ) {}

    public function __invoke(UserCancelledSubscriptionEvent $event)
    {
        $user = $event->getUser();

        $this->notificationsClient->postRequest("/{$user->getLocale()}/dealtabs/emails/subscription-cancelled", [
            'user' => [
                'email' => $user->getEmail(),
                'name' => $user->getName()
            ]
        ]);
    }
}