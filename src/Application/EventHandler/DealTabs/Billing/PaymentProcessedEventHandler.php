<?php

namespace App\Application\EventHandler\DealTabs\Billing;

use App\Application\Event\DealTabs\Billing\PaymentProcessedEvent;
use App\Infrastructure\ExternalService\NotificationsClient;
use App\Infrastructure\Messenger\EventBus\EventHandlerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PaymentProcessedEventHandler implements EventHandlerInterface
{
    public function __construct(
        private NotificationsClient $notificationsClient,
        private TranslatorInterface $translator
    ) {}

    public function __invoke(PaymentProcessedEvent $event)
    {
        $user = $event->getUser();

        $this->notificationsClient->postRequest("/{$user->getLocale()}/dealtabs/emails/payment-processed", [
            'user' => [
                'email' => $user->getEmail(),
                'name' => $user->getName()
            ]
        ]);

        $this->notificationsClient->postRequest('/dealtabs/notifications', [
            'type' => 'text',
            'context' => [
                'user' => [
                    'id' => $user->getId()
                ],
                'content' => $this->translator->trans('We have received your payment', [], 'messages', $user->getLocale())
            ]
        ]);
    }
}