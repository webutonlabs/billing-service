<?php

namespace App\Application\Event\DealTabs\Billing;

use App\Database\Domain\Entity\DealTabs\User\User;

class PaymentProcessedEvent
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}