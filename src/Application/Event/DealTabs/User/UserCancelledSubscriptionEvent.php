<?php

namespace App\Application\Event\DealTabs\User;

use App\Database\Domain\Entity\DealTabs\User\User;

class UserCancelledSubscriptionEvent
{
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}