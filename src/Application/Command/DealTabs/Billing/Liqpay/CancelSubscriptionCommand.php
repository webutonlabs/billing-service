<?php

namespace App\Application\Command\DealTabs\Billing\Liqpay;

class CancelSubscriptionCommand
{
    public int $subscription;

    public function __construct(int $subscription)
    {
        $this->subscription = $subscription;
    }
}