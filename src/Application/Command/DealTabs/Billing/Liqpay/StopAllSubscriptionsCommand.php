<?php

namespace App\Application\Command\DealTabs\Billing\Liqpay;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class StopAllSubscriptionsCommand
{
    public string $customerEmail;

    public function __construct(string $customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }
}