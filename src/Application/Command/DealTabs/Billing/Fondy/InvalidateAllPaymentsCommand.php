<?php

namespace App\Application\Command\DealTabs\Billing\Fondy;

class InvalidateAllPaymentsCommand
{
    public string $customerEmail;

    public function __construct(string $customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }
}