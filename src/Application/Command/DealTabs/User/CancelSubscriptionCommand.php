<?php

namespace App\Application\Command\DealTabs\User;

use Symfony\Component\Validator\Constraints as Assert;

class CancelSubscriptionCommand
{
    /**
     * @Assert\Email()
     */
    public string $user;

    public function __construct(string $user)
    {
        $this->user = $user;
    }
}