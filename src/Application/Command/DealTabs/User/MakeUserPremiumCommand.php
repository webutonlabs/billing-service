<?php

namespace App\Application\Command\DealTabs\User;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MakeUserPremiumCommand
{
    /**
     * @Assert\Email()
     */
    public string $user;

    public string $subscriptionType;
    public string $membershipType;
    public string $subscriptionExpires;
}