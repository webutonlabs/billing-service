<?php

namespace App\Application\CommandHandler\DealTabs\Billing\Liqpay;

use App\Application\Command\DealTabs\Billing\Liqpay\CancelSubscriptionCommand;
use App\Application\Command\DealTabs\User\CancelSubscriptionCommand as DealTabsCancelSubscription;
use App\Application\Event\DealTabs\User\UserCancelledSubscriptionEvent;
use App\Database\Domain\Entity\DealTabs\User\User;
use App\Database\Domain\Repository\DealTabs\UserRepository;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Billing\Liqpay\Sdk\LiqpaySdkFactory;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;
use App\Infrastructure\Messenger\EventBus\EventBusInterface;
use App\Infrastructure\Model\App;
use Doctrine\ORM\EntityManagerInterface;

class CancelSubscriptionCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private LiqpaySdkFactory $liqpaySdkFactory,
        private UserRepository $userRepository,
        private CommandBusInterface $commandBus,
        private EventBusInterface $eventBus,
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository
    ) {}

    public function __invoke(CancelSubscriptionCommand $command)
    {
        $liqpaySubscription = $this->liqpaySubscriptionRepository->findOneBy(['id' => $command->subscription]);
        $liqpaySubscription->setIsCancelled(true);

        $this->em->flush();

        $sdk = $this->liqpaySdkFactory->getLiqpaySdk();
        $sdk->api(
            'request',
            [
                'action' => 'unsubscribe',
                'version' => '3',
                'order_id' => $liqpaySubscription->getOrder()->getExternalId()
            ]
        );

        $customerEmail = $liqpaySubscription->getOrder()->getExternalData()['customer_email'];

        $this->commandBus->execute(new DealTabsCancelSubscription($customerEmail));

        /** @var User $user */
        $user = $this->userRepository->findOneBy(['email' => $customerEmail]);
        $this->eventBus->fire(new UserCancelledSubscriptionEvent($user));
    }
}