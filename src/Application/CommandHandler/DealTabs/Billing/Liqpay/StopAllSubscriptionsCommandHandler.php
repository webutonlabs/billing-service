<?php

namespace App\Application\CommandHandler\DealTabs\Billing\Liqpay;

use App\Application\Command\DealTabs\Billing\Liqpay\StopAllSubscriptionsCommand;
use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpaySubscription;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;
use App\Infrastructure\Model\App;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class StopAllSubscriptionsCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository
    ) {}

    public function __invoke(StopAllSubscriptionsCommand $command)
    {
        /** @var LiqpaySubscription[] $subscriptions */
        $subscriptions = $this->liqpaySubscriptionRepository->findBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $command->customerEmail
            ]
        );

        foreach ($subscriptions as $subscription) {
            $subscription->setIsCancelled(true);
            $subscription->setStopDate(new \DateTimeImmutable());
        }

        $this->em->flush();
    }
}