<?php

namespace App\Application\CommandHandler\DealTabs\Billing\Fondy;

use App\Application\Command\DealTabs\Billing\Fondy\StopAllSubscriptionsCommand;
use App\Database\Domain\Entity\Main\Billing\Fondy\FondySubscription;
use App\Database\Domain\Repository\Main\Fondy\FondySubscriptionRepository;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;
use App\Infrastructure\Model\App;
use Doctrine\ORM\EntityManagerInterface;

class StopAllSubscriptionsCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private FondySubscriptionRepository $fondySubscriptionRepository
    ) {}

    public function __invoke(StopAllSubscriptionsCommand $command)
    {
        /** @var FondySubscription[] $subscriptions */
        $subscriptions = $this->fondySubscriptionRepository->findBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $command->customerEmail,
                'stopDate' => null
            ]
        );

        foreach ($subscriptions as $subscription) {
            $subscription->setStopDate(new \DateTimeImmutable());
        }

        $this->em->flush();
    }
}