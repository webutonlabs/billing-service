<?php

namespace App\Application\CommandHandler\DealTabs\Billing\Fondy;

use App\Application\Command\DealTabs\Billing\Fondy\InvalidateAllPaymentsCommand;
use App\Database\Domain\Entity\Main\Billing\Fondy\FondyProcessedPayment;
use App\Database\Domain\Repository\Main\Fondy\FondyProcessedPaymentRepository;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;
use App\Infrastructure\Model\App;
use Doctrine\ORM\EntityManagerInterface;

class InvalidateAllPaymentsCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private FondyProcessedPaymentRepository $fondyProcessedPaymentRepository,
        private EntityManagerInterface $em
    ) {}

    public function __invoke(InvalidateAllPaymentsCommand $command)
    {
        /** @var FondyProcessedPayment[] $payments */
        $payments = $this->fondyProcessedPaymentRepository->findBy(
            [
                'app' => App::DEALTABS,
                'customerEmail' => $command->customerEmail
            ]
        );

        foreach ($payments as $payment) {
            $payment->invalidate();
        }

        $this->em->flush();
    }
}