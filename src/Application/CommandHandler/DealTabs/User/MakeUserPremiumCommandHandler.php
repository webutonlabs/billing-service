<?php

namespace App\Application\CommandHandler\DealTabs\User;

use App\Application\Command\DealTabs\User\MakeUserPremiumCommand;
use App\Infrastructure\ExternalService\DealTabsBillingClient;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class MakeUserPremiumCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private DealTabsBillingClient $billingClient
    ) {}

    public function __invoke(MakeUserPremiumCommand $command)
    {
        $this->billingClient->patchRequest('/make-user-premium', [
            'user' => $command->user,
            'subscription_type' => $command->subscriptionType,
            'membership_type' => $command->membershipType,
            'subscription_expires' => $command->subscriptionExpires
        ]);
    }
}