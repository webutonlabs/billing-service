<?php

namespace App\Application\CommandHandler\DealTabs\User;

use App\Application\Command\DealTabs\User\CancelSubscriptionCommand;
use App\Infrastructure\ExternalService\DealTabsBillingClient;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;

class CancelSubscriptionCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private DealTabsBillingClient $dealTabsBillingClient
    ) {}

    public function __invoke(CancelSubscriptionCommand $command)
    {
        $this->dealTabsBillingClient->patchRequest(
            '/cancel-subscription', ['user' => $command->user]
        );
    }
}