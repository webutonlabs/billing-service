<?php

namespace App\Application\CommandHandler\DealTabs\User;

use App\Application\Command\DealTabs\User\MakeUserBasicCommand;
use App\Infrastructure\ExternalService\DealTabsBillingClient;
use App\Infrastructure\Messenger\CommandBus\CommandHandlerInterface;

class MakeUserBasicCommandHandler implements CommandHandlerInterface
{
    public function __construct(
        private DealTabsBillingClient $dealTabsBillingClient
    ) {}

    public function __invoke(MakeUserBasicCommand $command)
    {
        $this->dealTabsBillingClient->patchRequest('/make-user-basic', ['user' => $command->user]);
    }
}