<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
trait CreationDateTrait
{
    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private \DateTimeInterface $creationDate;

    public function getCreationDate(): \DateTimeInterface
    {
        return $this->creationDate;
    }
}