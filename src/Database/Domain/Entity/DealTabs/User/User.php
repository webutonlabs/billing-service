<?php

namespace App\Database\Domain\Entity\DealTabs\User;

use App\Database\Domain\Entity\DealTabs\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 * @ORM\Table("users")
 */
class User extends AbstractEntity
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $locale;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }
}