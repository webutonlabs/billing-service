<?php

namespace App\Database\Domain\Entity\Main\Billing\Liqpay;

use App\Database\Domain\Entity\CreationDateTrait;
use App\Database\Domain\Entity\ExpiringDateTrait;
use App\Database\Domain\Entity\Main\AbstractEntity;
use App\Database\Domain\Entity\StopDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 */
class LiqpaySubscription extends AbstractEntity
{
    use CreationDateTrait;
    use ExpiringDateTrait;
    use StopDateTrait;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $app;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $customerEmail;

    /**
     * @ORM\ManyToOne(targetEntity="App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder")
     */
    private LiqpayOrder $order;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $isCancelled;

    public function __construct(
        string $app,
        LiqpayOrder $order,
        string $customerEmail,
        \DateTimeInterface $expiringDate
    ) {
        $this->app = $app;
        $this->order = $order;
        $this->customerEmail = $customerEmail;
        $this->isCancelled = false;
        $this->creationDate = new \DateTimeImmutable();
        $this->expiringDate = $expiringDate;
    }

    public function getApp(): string
    {
        return $this->app;
    }

    public function setApp(string $app): void
    {
        $this->app = $app;
    }

    public function getCustomerEmail(): string
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail(string $customerEmail): void
    {
        $this->customerEmail = $customerEmail;
    }

    public function getOrder(): LiqpayOrder
    {
        return $this->order;
    }

    public function setOrder(LiqpayOrder $order): void
    {
        $this->order = $order;
    }

    public function isCancelled(): bool
    {
        return $this->isCancelled;
    }

    public function setIsCancelled(bool $isCancelled): void
    {
        $this->isCancelled = $isCancelled;
    }
}