<?php

namespace App\Database\Domain\Entity\Main\Billing\Liqpay;

use App\Database\Domain\Entity\CreationDateTrait;
use App\Database\Domain\Entity\Main\AbstractEntity;
use App\Database\Domain\Entity\StopDateTrait;
use App\Database\Domain\Entity\ValidityDateTrait;
use App\Infrastructure\Model\BillingCycle;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 *
 * @ORM\Entity()
 * @ORM\Table("liqpay_orders")
 */
class LiqpayOrder extends AbstractEntity
{
    use CreationDateTrait;
    use StopDateTrait;
    use ValidityDateTrait;

    public const STATUS_CREATED = 1;
    public const STATUS_CREATED_SUBSCRIPTION = 2;
    public const STATUS_PROCESSED = 3;
    public const STATUS_PROCESSED_AND_EXPIRED = 4;

    public static array $statusNames = [
        self::STATUS_CREATED => 'status_created',
        self::STATUS_CREATED_SUBSCRIPTION => 'status_created_subscription',
        self::STATUS_PROCESSED => 'status_processed',
        self::STATUS_PROCESSED_AND_EXPIRED => 'status_processed_and_expired',
    ];

    /**
     * @ORM\Column(type="guid", nullable=false)
     */
    private string $externalId;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $liqpayPaymentId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $app;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $status;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $billingCycle;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    private array $externalData;

    public function __construct(string $externalId, string $app, int $billingCycle, array $externalData)
    {
        $this->externalId = $externalId;
        $this->app = $app;
        $this->externalData = $externalData;
        $this->status = self::STATUS_CREATED;
        $this->creationDate = new \DateTimeImmutable();
        $this->billingCycle = $billingCycle;
        $this->liqpayPaymentId = null;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function getApp(): string
    {
        return $this->app;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getBillingCycle(): int
    {
        return $this->billingCycle;
    }

    public function getExternalData(): array
    {
        return $this->externalData;
    }

    public function getLiqpayPaymentId(): ?int
    {
        return $this->liqpayPaymentId;
    }

    public function setLiqpayPaymentId(?int $liqpayPaymentId): void
    {
        $this->liqpayPaymentId = $liqpayPaymentId;
    }

    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function setApp(string $app): void
    {
        $this->app = $app;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function setBillingCycle(int $billingCycle): void
    {
        $this->billingCycle = $billingCycle;
    }

    public function setExternalData(array $externalData): void
    {
        $this->externalData = $externalData;
    }

    public function isRecurring()
    {
        return in_array($this->billingCycle, [BillingCycle::RECURRING_MONTHLY, BillingCycle::RECURRING_YEARLY]);
    }
}