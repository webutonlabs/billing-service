<?php

namespace App\Database\Domain\Entity\Main\Billing\Fondy;

use App\Database\Domain\Entity\CreationDateTrait;
use App\Database\Domain\Entity\ExpiringDateTrait;
use App\Database\Domain\Entity\Main\AbstractEntity;
use App\Database\Domain\Entity\StopDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class FondySubscription extends AbstractEntity
{
    use CreationDateTrait;
    use ExpiringDateTrait;
    use StopDateTrait;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $externalId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $app;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $billingCycle;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $customerEmail;

    public function __construct(
        string $app,
        string $externalId,
        string $customerEmail,
        int $billingCycle,
        \DateTimeInterface $expiringDate
    ) {
        $this->app = $app;
        $this->externalId = $externalId;
        $this->customerEmail = $customerEmail;
        $this->billingCycle = $billingCycle;
        $this->expiringDate = $expiringDate;
        $this->creationDate = new \DateTimeImmutable();
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getApp(): string
    {
        return $this->app;
    }

    public function setApp(string $app): void
    {
        $this->app = $app;
    }

    public function getBillingCycle(): int
    {
        return $this->billingCycle;
    }

    public function setBillingCycle(int $billingCycle): void
    {
        $this->billingCycle = $billingCycle;
    }

    public function getCustomerEmail(): string
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail(string $customerEmail): void
    {
        $this->customerEmail = $customerEmail;
    }
}