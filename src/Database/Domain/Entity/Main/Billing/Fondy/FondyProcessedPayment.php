<?php

namespace App\Database\Domain\Entity\Main\Billing\Fondy;

use App\Database\Domain\Entity\CreationDateTrait;
use App\Database\Domain\Entity\Main\AbstractEntity;
use App\Database\Domain\Entity\ValidityDateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Used to save data about one-time payments (not subscriptions)
 *
 * @ORM\Entity()
 */
class FondyProcessedPayment extends AbstractEntity
{
    use CreationDateTrait;
    use ValidityDateTrait;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $externalId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $app;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $billingCycle;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $customerEmail;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $isInvalidated;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeInterface $invalidatedDate;

    public function __construct(
        string $app,
        string $externalId,
        string $customerEmail,
        int $billingCycle,
        \DateTimeInterface $validityDate
    ) {
        $this->app = $app;
        $this->externalId = $externalId;
        $this->customerEmail = $customerEmail;
        $this->billingCycle = $billingCycle;
        $this->validityDate = $validityDate;
        $this->creationDate = new \DateTimeImmutable();
        $this->isInvalidated = false;
        $this->invalidatedDate = null;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getApp(): string
    {
        return $this->app;
    }

    public function setApp(string $app): void
    {
        $this->app = $app;
    }

    public function getBillingCycle(): int
    {
        return $this->billingCycle;
    }

    public function setBillingCycle(int $billingCycle): void
    {
        $this->billingCycle = $billingCycle;
    }

    public function getCustomerEmail(): string
    {
        return $this->customerEmail;
    }

    public function setCustomerEmail(string $customerEmail): void
    {
        $this->customerEmail = $customerEmail;
    }

    public function isInvalidated(): bool
    {
        return $this->isInvalidated;
    }

    public function setIsInvalidated(bool $isInvalidated): void
    {
        $this->isInvalidated = $isInvalidated;
    }

    public function getInvalidatedDate(): ?\DateTimeInterface
    {
        return $this->invalidatedDate;
    }

    public function setInvalidatedDate(?\DateTimeInterface $invalidatedDate): void
    {
        $this->invalidatedDate = $invalidatedDate;
    }

    public function invalidate()
    {
        $this->isInvalidated = true;
        $this->invalidatedDate = new \DateTimeImmutable();
    }
}