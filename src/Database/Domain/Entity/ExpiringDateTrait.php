<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ExpiringDateTrait
{
    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private \DateTimeInterface $expiringDate;

    public function getExpiringDate(): \DateTimeInterface
    {
        return $this->expiringDate;
    }

    public function setExpiringDate(\DateTimeInterface $expiringDate): void
    {
        $this->expiringDate = $expiringDate;
    }
}