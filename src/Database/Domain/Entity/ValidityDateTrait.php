<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ValidityDateTrait
{
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeInterface $validityDate = null;

    public function getValidityDate(): ?\DateTimeInterface
    {
        return $this->validityDate;
    }

    public function setValidityDate(?\DateTimeInterface $validityDate): void
    {
        $this->validityDate = $validityDate;
    }
}