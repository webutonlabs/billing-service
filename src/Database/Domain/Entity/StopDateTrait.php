<?php

namespace App\Database\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

trait StopDateTrait
{
    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private ?\DateTimeInterface $stopDate = null;

    public function getStopDate(): ?\DateTimeInterface
    {
        return $this->stopDate;
    }

    public function setStopDate(?\DateTimeInterface $stopDate): void
    {
        $this->stopDate = $stopDate;
    }
}