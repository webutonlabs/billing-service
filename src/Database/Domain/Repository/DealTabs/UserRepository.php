<?php

namespace App\Database\Domain\Repository\DealTabs;

use App\Database\Domain\Entity\DealTabs\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
}