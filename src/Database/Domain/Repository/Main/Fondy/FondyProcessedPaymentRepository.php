<?php

namespace App\Database\Domain\Repository\Main\Fondy;

use App\Database\Domain\Entity\Main\Billing\Fondy\FondyProcessedPayment;
use App\Infrastructure\Model\BillingCycle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FondyProcessedPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FondyProcessedPayment::class);
    }

    public function listNotInvalidatedProcessedPayments(string $app): array
    {
        $qb = $this->createQueryBuilder('fp');
        $qb->where('fp.billingCycle IN (:cycles)');
        $qb->setParameter('cycles', BillingCycle::getOneTimeCycles());

        $qb->andWhere('fp.app = :app_name');
        $qb->setParameter('app_name', $app);

        $qb->andWhere("fp.isInvalidated = :is_invalidated");
        $qb->setParameter('is_invalidated', false);

        $qb->andWhere("fp.validityDate <= :compare_date");
        $qb->setParameter('compare_date', new \DateTimeImmutable('now'));

        return $qb->getQuery()->getResult();
    }
}