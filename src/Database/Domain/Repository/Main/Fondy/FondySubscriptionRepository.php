<?php

namespace App\Database\Domain\Repository\Main\Fondy;

use App\Database\Domain\Entity\Main\Billing\Fondy\FondySubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FondySubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FondySubscription::class);
    }

    public function findNotStoppedAndExpired(string $app): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where('s.app = :app_name');
        $qb->setParameter('app_name', $app);

        $qb->andWhere("s.expiringDate <= :compare_date");
        $qb->setParameter('compare_date', new \DateTimeImmutable('now - 3 days')); // payment issues or something

        $qb->andWhere('s.stopDate IS NULL');

        return $qb->getQuery()->getResult();
    }
}