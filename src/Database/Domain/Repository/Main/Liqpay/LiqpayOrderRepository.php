<?php

namespace App\Database\Domain\Repository\Main\Liqpay;

use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class LiqpayOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LiqpayOrder::class);
    }

    public function listInvalidatedAndNotStoppedOneTimePaymentOrders(string $app): array
    {
        $qb = $this->createQueryBuilder('o');
        $qb->where('o.billingCycle IN (:cycles)');
        $qb->setParameter('cycles', [LiqpayOrder::BILLING_CYCLE_ONE_TIME_MONTHLY, LiqpayOrder::BILLING_CYCLE_ONE_TIME_YEARLY]);

        $qb->andWhere('o.app = :app_name');
        $qb->setParameter('app_name', $app);

        $qb->andWhere('o.stopDate IS NULL');
        $qb->andWhere('o.validityDate IS NOT NULL');

        $qb->andWhere("o.validityDate <= :compare_date");
        $qb->setParameter('compare_date', new \DateTimeImmutable('now'));

        return $qb->getQuery()->getResult();
    }
}