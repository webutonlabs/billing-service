<?php

namespace App\Database\Domain\Repository\Main\Liqpay;

use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpaySubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class LiqpaySubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LiqpaySubscription::class);
    }

    public function findNotStoppedAndExpired(string $app): array
    {
        $qb = $this->createQueryBuilder('s');
        $qb->where('s.app = :app_name');
        $qb->setParameter('app_name', $app);

        $qb->andWhere("s.expiringDate <= :compare_date");
        $qb->setParameter('compare_date', new \DateTimeImmutable('now - 3 days')); // payment issues or something

        $qb->andWhere('s.stopDate IS NULL');

        return $qb->getQuery()->getResult();
    }
}