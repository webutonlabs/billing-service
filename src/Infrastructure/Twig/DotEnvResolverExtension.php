<?php

namespace App\Infrastructure\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class DotEnvResolverExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [new TwigFunction('env', [$this, 'env'])];
    }

    public function env(string $key)
    {
        return $_ENV[$key];
    }
}