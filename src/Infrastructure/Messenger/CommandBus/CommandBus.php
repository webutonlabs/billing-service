<?php
declare(strict_types=1);

namespace App\Infrastructure\Messenger\CommandBus;

use Symfony\Component\Messenger\MessageBusInterface;

class CommandBus implements CommandBusInterface
{
    public function __construct(
        private MessageBusInterface $messageBus
    ) {}

    public function execute($command): void
    {
        $this->messageBus->dispatch($command);
    }
}
