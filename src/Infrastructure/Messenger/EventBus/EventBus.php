<?php
declare(strict_types=1);

namespace App\Infrastructure\Messenger\EventBus;

use Symfony\Component\Messenger\MessageBusInterface;

class EventBus implements EventBusInterface
{
    public function __construct(
        private MessageBusInterface $messageBus
    ) {}

    public function fire($event): void
    {
        $this->messageBus->dispatch($event);
    }
}
