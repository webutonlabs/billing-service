<?php
declare(strict_types=1);

namespace App\Infrastructure\Messenger\EventBus;

interface EventBusInterface
{
    public function fire($event): void;
}
