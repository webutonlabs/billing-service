<?php

namespace App\Infrastructure\EventListener;

use App\Infrastructure\Service\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class ExceptionListener
{
    public function __construct(
        private Logger $logger
    ) {}

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse(['message' => $exception->getMessage()], 500);

        if ($exception instanceof AccessDeniedException) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 403);
        }

        if ($exception instanceof BadRequestHttpException) {
            $response = new JsonResponse(['message' => $exception->getMessage()], 400);
        }

        $this->logger->error($exception->getMessage(), [
            'trace' => $exception->getTrace(),
            'headers' => $event->getRequest()->headers->all(),
            'body' => json_decode($event->getRequest()->getContent(), true),
            'form_data' => $event->getRequest()->request->all(),
            'query' => $event->getRequest()->query->all(),
            'request' => [
                'ip' => $event->getRequest()->getClientIp(),
                'path_info' => $event->getRequest()->getPathInfo(),
                'request_uri' => $event->getRequest()->getRequestUri()
            ]
        ]);

        $event->setResponse($response);
    }
}