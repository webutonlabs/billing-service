<?php

namespace App\Infrastructure\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class AccessChecker
{
    public static function passOrThrow(Request $request)
    {
        if ($request->headers->get('X-AUTH-TOKEN') !== $_ENV['ACCESS_TOKEN']) {
            throw new AccessDeniedException();
        }
    }
}