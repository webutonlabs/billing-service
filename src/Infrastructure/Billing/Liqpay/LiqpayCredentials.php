<?php

namespace App\Infrastructure\Billing\Liqpay;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class LiqpayCredentials
{
    public string $publicKey;
    public string $privateKey;
    public string $testPublicKey;
    public string $testPrivateKey;
    public string $env;

    public function __construct()
    {
        $this->publicKey = $_ENV['LIQPAY_DEALTABS_PUBLIC_KEY'];
        $this->privateKey = $_ENV['LIQPAY_DEALTABS_PRIVATE_KEY'];
        $this->testPublicKey = $_ENV['LIQPAY_DEALTABS_TEST_PUBLIC_KEY'];
        $this->testPrivateKey = $_ENV['LIQPAY_DEALTABS_TEST_PRIVATE_KEY'];
        $this->env = $_ENV['LIQPAY_DEALTABS_ENV'];
    }

    public function getEnvPublicKey()
    {
        if ($this->env === 'test') {
            return $this->testPublicKey;
        }

        return $this->publicKey;
    }

    public function getEnvPrivateKey()
    {
        if ($this->env === 'test') {
            return $this->testPrivateKey;
        }

        return $this->privateKey;
    }
}