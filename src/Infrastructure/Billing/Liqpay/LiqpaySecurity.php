<?php

namespace App\Infrastructure\Billing\Liqpay;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class LiqpaySecurity
{
    public static function checkSignature(string $signature, string $data, string $privateKey): bool
    {
        $sign = base64_encode( sha1($privateKey . $data . $privateKey, 1 ));
        return $sign === $signature;
    }
}