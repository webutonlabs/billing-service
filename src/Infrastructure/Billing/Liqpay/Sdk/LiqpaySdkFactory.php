<?php

namespace App\Infrastructure\Billing\Liqpay\Sdk;

use App\Infrastructure\Billing\Liqpay\LiqpayCredentials;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class LiqpaySdkFactory
{
    private LiqpaySdk $liqpaySdk;

    public function __construct(LiqpayCredentials $credentials)
    {
        $this->liqpaySdk = new LiqpaySdk($credentials->getEnvPublicKey(), $credentials->getEnvPrivateKey());
    }

    public function getLiqpaySdk(): LiqpaySdk
    {
        return $this->liqpaySdk;
    }
}