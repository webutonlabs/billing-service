<?php

namespace App\Infrastructure\Billing\Fondy;

class FondyCredentials
{
    public static function getEnvPaymentKey()
    {
        if ($_ENV['FONDY_DEALTABS_ENV'] === 'test') {
            return 'test';
        }

        return $_ENV['FONDY_DEALTABS_PAYMENT_KEY'];
    }

    public static function getEnvCreditKey()
    {
        if ($_ENV['FONDY_DEALTABS_ENV'] === 'test') {
            return 'test';
        }

        return $_ENV['FONDY_DEALTABS_CREDIT_KEY'];
    }

    public static function getEnvMerchantId()
    {
        if ($_ENV['FONDY_DEALTABS_ENV'] === 'test') {
            return 1396424; // see fondy docs
        }

        return $_ENV['FONDY_DEALTABS_MERCHANT_ID'];
    }
}