<?php

namespace App\Infrastructure\Billing\Fondy\Sdk;

use Cloudipsp\Helper as Helper;

class FondySignature
{
    private static string $paymentKey;
    private static string $merchantId;

    public static function paymentKey($paymentKey)
    {
        self::$paymentKey = $paymentKey;
    }

    public static function merchantId($merchantId)
    {
        self::$merchantId = $merchantId;
    }

    public static function check(array $response)
    {
        if (!array_key_exists('signature', $response)) {
            return false;
        }

        return $response['signature'] === Helper\ApiHelper::generateSignature($response["data"], self::$paymentKey, '2.0') ;
    }
}