<?php

namespace App\Infrastructure\Billing\Fondy\Sdk;

use App\Infrastructure\Billing\Fondy\FondyCredentials;
use Cloudipsp\Configuration;

class FondySdkFactory
{
    public static function setCredentials()
    {
        Configuration::setMerchantId(FondyCredentials::getEnvMerchantId());
        Configuration::setSecretKey(FondyCredentials::getEnvPaymentKey());
        Configuration::setApiVersion('2.0');
    }
}