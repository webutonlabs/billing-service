<?php

namespace App\Infrastructure\ExternalService;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\HttpClient;

class LogsServiceClient
{
    private string $accessToken;
    private string $logsHost;

    public function __construct(private ParameterBagInterface $parameterBag)
    {
        $this->accessToken = $_ENV['LOGS_SERVICE_ACCESS_TOKEN'];
        $this->logsHost = $_ENV['LOGS_SERVICE_HOST'];
    }

    public function postRequest(string $uri, array $data): void
    {
        try {
            HttpClient::create()->request(
                'POST',
                $this->logsHost . $uri,
                [
                    'headers' => [
                        'X-AUTH-TOKEN' => $this->accessToken,
                        'X-SERVICE' => 'billing_service',
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($data)
                ]
            );
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }
}