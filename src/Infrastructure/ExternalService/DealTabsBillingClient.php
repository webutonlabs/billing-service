<?php

namespace App\Infrastructure\ExternalService;

use Symfony\Component\HttpClient\HttpClient;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class DealTabsBillingClient
{
    private string $accessToken;
    private string $apiHost;

    public function __construct()
    {
        $this->accessToken = $_ENV['DEALTABS_BILLING_ACCESS_TOKEN'];
        $this->apiHost = $_ENV['DEALTABS_API_HOST'];
    }

    public function patchRequest(string $uri, array $data): void
    {
        HttpClient::create()->request(
            'PATCH',
            $this->apiHost . '/billing' . $uri,
            [
                'headers' => [
                    'X-BILLING-TOKEN' => $this->accessToken,
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($data)
            ]
        );
    }
}