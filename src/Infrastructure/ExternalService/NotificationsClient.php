<?php

namespace App\Infrastructure\ExternalService;

use App\Infrastructure\Service\Logger;
use Symfony\Component\HttpClient\HttpClient;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class NotificationsClient
{
    private string $accessToken;
    private string $notificationsHost;

    public function __construct(private Logger $logger)
    {
        $this->accessToken = $_ENV['NOTIFICATIONS_ACCESS_TOKEN'];
        $this->notificationsHost = $_ENV['NOTIFICATIONS_SERVICE_HOST'];
    }

    public function postRequest(string $uri, array $data): void
    {
        try {
            HttpClient::create()->request(
                'POST',
                $this->notificationsHost . $uri,
                [
                    'headers' => [
                        'X-AUTH-TOKEN' => $this->accessToken,
                        'Content-Type' => 'application/json'
                    ],
                    'body' => json_encode($data)
                ]
            );
        } catch (\Exception $e) {
            $this->logger->error('Failed to use notifications service', [
                'message' => $e->getMessage(),
                'data' => $data,
                'uri' => $uri
            ]);
        }
    }
}