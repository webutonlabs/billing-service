<?php

namespace App\Infrastructure\Service;

use App\Infrastructure\ExternalService\LogsServiceClient;

class Logger
{
    public function __construct(
        private LogsServiceClient $client
    ) {}

    public function info(string $message, array $data = [])
    {
        $this->client->postRequest('/logs', ['type' => 'info', 'main_message' => $message, 'data' => $data]);
    }

    public function warning(string $message, array $data = [])
    {
        $this->client->postRequest('/logs', ['type' => 'warning', 'main_message' => $message, 'data' => $data]);
    }

    public function error(string $message, array $data = [])
    {
        $this->client->postRequest('/logs', ['type' => 'error', 'main_message' => $message, 'data' => $data]);
    }
}