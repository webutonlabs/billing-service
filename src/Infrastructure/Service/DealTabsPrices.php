<?php

namespace App\Infrastructure\Service;

use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder;
use App\Infrastructure\Model\BillingCycle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @author Anton Zakharuk <anton.zakharuk@cosmonova.net>
 */
class DealTabsPrices
{
    private float $oneTimeMonthly;
    private float $oneTimeYearly;
    private float $recurringMonthly;
    private float $recurringYearly;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $data = $parameterBag->get('dealtabs_premium');

        $this->oneTimeMonthly = $data['one_time_monthly'];
        $this->oneTimeYearly = $data['one_time_yearly'];
        $this->recurringMonthly = $data['recurring_monthly'];
        $this->recurringYearly = $data['recurring_yearly'];
    }

    public function getFromBillingCycle(int $cycle)
    {
        return [
            BillingCycle::ONE_TIME_MONTHLY => $this->oneTimeMonthly,
            BillingCycle::ONE_TIME_YEARLY => $this->oneTimeYearly,
            BillingCycle::RECURRING_MONTHLY => $this->recurringMonthly,
            BillingCycle::RECURRING_YEARLY => $this->recurringYearly,
        ][$cycle];
    }
}