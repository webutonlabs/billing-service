<?php

namespace App\Infrastructure\Model;

class BillingCycle
{
    public const ONE_TIME_MONTHLY = 1;
    public const ONE_TIME_YEARLY = 2;
    public const RECURRING_MONTHLY = 3;
    public const RECURRING_YEARLY = 4;

    public static array $billingCycleNames = [
        self::ONE_TIME_MONTHLY => 'one_time_monthly',
        self::ONE_TIME_YEARLY => 'one_time_yearly',
        self::RECURRING_MONTHLY => 'recurring_monthly',
        self::RECURRING_YEARLY => 'recurring_yearly',
    ];

    public static function getRecurringCycles(): array
    {
        return [self::RECURRING_MONTHLY, self::RECURRING_YEARLY];
    }

    public static function getOneTimeCycles(): array
    {
        return [self::ONE_TIME_YEARLY, self::ONE_TIME_MONTHLY];
    }

    public static function all(): array
    {
        return [...self::getOneTimeCycles(), ...self::getRecurringCycles()];
    }
}