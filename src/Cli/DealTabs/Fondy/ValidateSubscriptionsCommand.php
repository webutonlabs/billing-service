<?php

namespace App\Cli\DealTabs\Fondy;

use App\Application\Command\DealTabs\User\MakeUserBasicCommand as DealTabsMakeUserBasicCommand;
use App\Database\Domain\Entity\Main\Billing\Fondy\FondyProcessedPayment;
use App\Database\Domain\Entity\Main\Billing\Fondy\FondySubscription;
use App\Database\Domain\Repository\Main\Fondy\FondyProcessedPaymentRepository;
use App\Database\Domain\Repository\Main\Fondy\FondySubscriptionRepository;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use App\Infrastructure\Model\App;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ValidateSubscriptionsCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $em,
        private CommandBusInterface $commandBus,
        private FondySubscriptionRepository $fondySubscriptionRepository,
        private FondyProcessedPaymentRepository $fondyProcessedPaymentRepository
    ) {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('dealtabs:fondy:validate-subscriptions');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('Starting...');

        try {
            /** @var FondySubscription[] $expiredSubscriptions */
            $expiredSubscriptions = $this->fondySubscriptionRepository->findNotStoppedAndExpired(App::DEALTABS);

            foreach ($expiredSubscriptions as $expiredSubscription) {
                $expiredSubscription->setStopDate(new \DateTimeImmutable());
                $this->commandBus->execute(new DealTabsMakeUserBasicCommand($expiredSubscription->getCustomerEmail()));
            }

            /** @var FondyProcessedPayment[] $payments */
            $payments = $this->fondyProcessedPaymentRepository->listNotInvalidatedProcessedPayments(App::DEALTABS);

            foreach ($payments as $payment) {
                $payment->invalidate();

                $this->commandBus->execute(new DealTabsMakeUserBasicCommand($payment->getCustomerEmail()));
            }

            $this->em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->success('Finished.');

        return 1;
    }
}