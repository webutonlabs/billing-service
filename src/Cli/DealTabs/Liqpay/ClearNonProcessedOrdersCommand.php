<?php

namespace App\Cli\DealTabs\Liqpay;

use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ClearNonProcessedOrdersCommand extends Command
{
    public function __construct(private EntityManagerInterface $em)
    {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('dealtabs:liqpay:clear-non-processed-orders');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('Starting...');

        try {
            $query = "DELETE FROM liqpay_orders 
                WHERE creation_date <= now() - INTERVAL '14 DAYS'
                AND liqpay_orders.status = :status
            ";

            $this->em->getConnection()->executeQuery($query, ['status' => LiqpayOrder::STATUS_CREATED]);
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->success('Finished');

        return 1;
    }
}