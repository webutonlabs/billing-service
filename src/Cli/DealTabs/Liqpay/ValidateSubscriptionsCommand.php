<?php

namespace App\Cli\DealTabs\Liqpay;

use App\Application\Command\DealTabs\User\MakeUserBasicCommand as DealTabsMakeUserBasicCommand;
use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpayOrder;
use App\Database\Domain\Entity\Main\Billing\Liqpay\LiqpaySubscription;
use App\Database\Domain\Repository\Main\Liqpay\LiqpayOrderRepository;
use App\Database\Domain\Repository\Main\Liqpay\LiqpaySubscriptionRepository;
use App\Infrastructure\Messenger\CommandBus\CommandBusInterface;
use App\Infrastructure\Model\App;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ValidateSubscriptionsCommand extends Command
{
    public function __construct(
        private EntityManagerInterface $em,
        private LiqpaySubscriptionRepository $liqpaySubscriptionRepository,
        private LiqpayOrderRepository $liqpayOrderRepository,
        private CommandBusInterface $commandBus
    ) {
        parent::__construct();
    }

    public function configure()
    {
        $this->setName('dealtabs:liqpay:validate-subscriptions');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('Starting...');

        try {
            /** @var LiqpaySubscription[] $expiredSubscriptions */
            $expiredSubscriptions = $this->liqpaySubscriptionRepository->findNotStoppedAndExpired(App::DEALTABS);

            foreach ($expiredSubscriptions as $expiredSubscription) {
                $expiredSubscription->setIsCancelled(true);
                $expiredSubscription->setStopDate(new \DateTimeImmutable());

                $this->commandBus->execute(new DealTabsMakeUserBasicCommand($expiredSubscription->getCustomerEmail()));
            }

            /** @var LiqpayOrder[] $oneTimePaymentOrders */
            $oneTimePaymentOrders = $this->liqpayOrderRepository->listInvalidatedAndNotStoppedOneTimePaymentOrders(App::DEALTABS);

            foreach ($oneTimePaymentOrders as $order) {
                $order->setStopDate(new \DateTimeImmutable());

                $customerEmail = $order->getExternalData()['customer_email'];
                $this->commandBus->execute(new DealTabsMakeUserBasicCommand($customerEmail));
            }

            $this->em->flush();
        } catch (\Exception $e) {
            $io->error($e->getMessage());
        }

        $io->success('Finished.');

        return 1;
    }
}