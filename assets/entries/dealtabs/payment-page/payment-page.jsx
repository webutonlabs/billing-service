import './styles/main.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import 'react-toastify/dist/ReactToastify.css';
import PaymentForm from "./components/PaymentForm";

ReactDOM.render(
    <PaymentForm />,
    document.getElementById('payment-form-container'),
);