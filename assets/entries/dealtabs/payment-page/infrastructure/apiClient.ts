import axios from "axios";

declare var LOCALE: string;

export const apiClient = axios.create({
    baseURL: `/${LOCALE}/dealtabs`,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
    },
});