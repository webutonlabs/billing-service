import Loader from "./Loader";
import React from "react";

const AppLoader = () => {
    return (
        <div
            style={{
                display: 'flex',
                width: '100%',
                height: '15vh',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Loader/>
        </div>
    )
}
export default AppLoader;