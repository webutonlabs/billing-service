import React from "react";
import {FaArrowRight} from "react-icons/all";

class ChoosePaymentType extends React.Component {
    handleChange = (event) => {
        this.props.setPaymentType(Number(event.target.value));
    }

    render() {
        return (
            <div className="col-lg-12 col-12">
                <div className="card shadow mt-2">
                    <div className="card-header">
                        <h5>{PAYMENT_TYPE_MESSAGE}</h5>
                    </div>
                    <div className="card-body">
                        <div className="option-group">
                            <div className="option-container">
                                <input
                                    onChange={this.handleChange}
                                    checked={this.props.paymentType === 1}
                                    value={1}
                                    className="option-input"
                                    id="payment-type-fondy"
                                    type="radio"
                                />
                                <input
                                    onChange={this.handleChange}
                                    checked={this.props.paymentType === 2}
                                    value={2}
                                    className="option-input"
                                    id="payment-type-skrill"
                                    type="radio"
                                />
                                <label className="option" htmlFor="payment-type-fondy">
                                    <span className="option__indicator"/>
                                    <span className="option__label">
                                        <img src="/static/billing/fondy_logo_square.png" alt="fondy" className="mb-1 logo-img"/>
                                        <span className={'ml-1'}>Fondy</span>
                                    </span>
                                </label>
                                <label className="option" htmlFor="payment-type-skrill">
                                    <span className="option__indicator"/>
                                    <span className="option__label">
                                        <img src="/static/billing/skrill_logo.png" alt="skrill" className="mb-1 logo-img"/>
                                        <span className={'ml-1'}>Skrill</span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div className={'card-footer text-end'}>
                        <button onClick={this.props.nextStep} className={'btn btn-success'}>
                            <span>{NEXT}</span>
                            <span style={{marginLeft: '3px'}}><FaArrowRight /></span>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ChoosePaymentType;