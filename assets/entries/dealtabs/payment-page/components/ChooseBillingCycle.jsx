import React from "react";
import {FaArrowLeft, FaArrowRight} from "react-icons/all";

class ChooseBillingCycle extends React.Component {
    handleChange = (event) => {
        this.props.setBillingCycle(Number(event.target.value));
    }

    render() {
        return (
            <div className="col-lg-12 col-12">
                <div className="card shadow">
                    <div className="card-header">
                        <h5>{BILLING_CYCLE_MESSAGE}</h5>
                    </div>
                    <div className="card-body">
                        <div className="row justify-content-center">
                            <div className="col-lg-5 col-md-10 col-12">
                                <div className="inputGroup">
                                    <input
                                        onChange={this.handleChange}
                                        checked={this.props.billingCycle === 1}
                                        value={1}
                                        id="radio1"
                                        type="radio"
                                    />
                                    <label htmlFor="radio1">
                                        <span>{ONE_TIME_MONTH_MESSAGE}</span>
                                        <span className="price ml-1">2.49$</span>
                                    </label>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-10 col-12">
                                <div className="inputGroup">
                                    <input
                                        onChange={this.handleChange}
                                        checked={this.props.billingCycle === 2}
                                        value={2}
                                        id="radio2"
                                        type="radio"
                                    />
                                    <label htmlFor="radio2">
                                        <span>{ONE_TIME_YEAR_MESSAGE}</span>
                                        <span className="price ml-1">22.99$</span>
                                    </label>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-10 col-12">
                                <div className="inputGroup">
                                    <input
                                        onChange={this.handleChange}
                                        checked={this.props.billingCycle === 3}
                                        value={3}
                                        id="radio3"
                                        type="radio"
                                    />
                                    <label htmlFor="radio3">
                                        <span>{RECURRING_MONTH_MESSAGE}</span>
                                        <span className="price ml-1">2.29$</span>
                                    </label>
                                </div>
                            </div>
                            <div className="col-lg-5 col-md-10 col-12">
                                <div className="inputGroup">
                                    <input
                                        onChange={this.handleChange}
                                        checked={this.props.billingCycle === 4}
                                        value={4}
                                        id="radio4"
                                        type="radio"
                                    />
                                    <label htmlFor="radio4">
                                        <span>{RECURRING_YEAR_MESSAGE}</span>
                                        <span className="price ml-1">20.99$</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={'card-footer d-flex justify-content-end'}>
                        {/*<button onClick={this.props.prevStep} className={'btn btn-primary'}>*/}
                        {/*    <FaArrowLeft />*/}
                        {/*    <span style={{marginLeft: '3px'}}>{BACK}</span>*/}
                        {/*</button>*/}
                        <button onClick={this.props.nextStep} className={'btn btn-success'}>
                            <span>{NEXT}</span>
                            <span style={{marginLeft: '3px'}}><FaArrowRight /></span>
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default ChooseBillingCycle;