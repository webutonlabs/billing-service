import React from "react";
import {FaArrowLeft} from "react-icons/all";
import FondyForm from "./billing/FondyForm";

class ProcessPayment extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="col-lg-12 col-12">
                <div className="card shadow mt-2">
                    <div className="card-header">
                        <h5>{PROCESS_PAYMENT}</h5>
                    </div>
                    <div className="card-body">
                        {this.props.billingCycle === 1 &&
                            <div className={'alert alert-primary'}>
                                <div>{PROCESS_ALERT_1}</div>
                            </div>
                        }
                        {this.props.billingCycle === 2 &&
                            <div className={'alert alert-primary'}>
                                <div>{PROCESS_ALERT_2}</div>
                            </div>
                        }
                        {this.props.billingCycle === 3 &&
                            <div className={'alert alert-primary'}>
                                <div>{PROCESS_ALERT_3}</div>
                            </div>
                        }
                        {this.props.billingCycle === 4 &&
                            <div className={'alert alert-primary'}>
                                <div>{PROCESS_ALERT_4}</div>
                            </div>
                        }
                        {this.props.paymentType === 1 &&
                            <FondyForm billingCycle={this.props.billingCycle} email={this.props.email} />
                        }
                    </div>
                    <div className={'card-footer d-flex justify-content-between'}>
                        <button onClick={this.props.prevStep} className={'btn btn-primary'}>
                            <FaArrowLeft />
                            <span style={{marginLeft: '3px'}}>{BACK}</span>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProcessPayment;