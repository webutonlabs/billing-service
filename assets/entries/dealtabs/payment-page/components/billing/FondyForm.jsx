import React from 'react';

class FondyForm extends React.Component {
    render() {
        return (
            <form method={'POST'} action={`/${LOCALE}/dealtabs/fondy/redirect-to-checkout`}>
                <input type={'hidden'} name={'email'} value={this.props.email}/>
                <input type={'hidden'} name={'billing_cycle'} value={this.props.billingCycle}/>
                <button className={'btn btn-success'} type={"submit"}>{GO_TO_CHECKOUT_MESSAGE}</button>
            </form>
        )
    }
}

export default FondyForm;