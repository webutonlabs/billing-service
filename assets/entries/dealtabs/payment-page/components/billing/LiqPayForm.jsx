import React from "react";
import AppLoader from "../../infrastructure/Loader/AppLoader";
import {apiClient} from "../../infrastructure/apiClient";

class LiqPayForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            data: '',
            signature: ''
        }
    }

    componentDidMount() {
        apiClient.post('/liqpay/create-order',
            {email: this.props.email, billing_cycle: this.props.billingCycle}
        ).then(res => {
            this.setState({
                data: res.data.data,
                signature: res.data.signature,
                isLoading: false
            });
        });
    }

    render() {
        return (
            this.state.isLoading ? <AppLoader /> :
                <form method="POST" action="https://www.liqpay.ua/api/3/checkout" acceptCharset="utf-8">
                    <input type="hidden" name="data" value={this.state.data} />
                    <input type="hidden" name="signature" value={this.state.signature} />
                    <input className={'btn btn-success'} type="submit" value={GO_TO_CHECKOUT_MESSAGE}/>
                </form>
        )
    }
}

export default LiqPayForm;