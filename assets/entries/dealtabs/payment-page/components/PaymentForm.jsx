import React from "react";
import ChooseBillingCycle from "./ChooseBillingCycle";
import ChoosePaymentType from "./ChoosePaymentType";
import {apiClient} from "../infrastructure/apiClient";
import ProcessPayment from "./ProcessPayment";

class PaymentForm extends React.Component {
    constructor(props) {
        super(props);

        const urlSearchParams = new URLSearchParams(window.location.search);
        if (null === urlSearchParams.get('email')) {
            window.location.href = UI_HOST;
        }

        const email = urlSearchParams.get('email').replace(/ /g,"+"); // for ex test+01@gmail.com passed as parameter

        this.state =  {
            email: email,
            billingCycle: 1,
            paymentType: 1,
            step: 2
        }
    }

    componentDidMount() {
        if (!this.state.email) {
            window.location.href = UI_HOST;
        }

        apiClient.get(`/check-subscription-existence?email=${this.state.email}`).then(() => {
            window.location.href = `${UI_HOST}/membership?toast=error&message=${CANCEL_SUBSCRIPTION_MESSAGE}`;
        });
    }

    setBillingCycle = (cycle) => {
        this.setState({billingCycle: cycle});
    }

    setPaymentType = (type) => {
        this.setState({paymentType: type});
    }

    nextStep = () => {
        this.setState({step: ++this.state.step});
    }

    prevStep = () => {
        this.setState({step: --this.state.step});
    }

    render() {
        return (
            <div className="row justify-content-center">
                <div className="col-lg-12 col-12">
                    <div className="row justify-content-center text-center">
                        {this.state.step === 1 &&
                            <ChoosePaymentType
                                paymentType={this.state.paymentType}
                                setPaymentType={this.setPaymentType}
                                nextStep={this.nextStep}
                            />
                        }
                        {this.state.step === 2 &&
                            <ChooseBillingCycle
                                billingCycle={this.state.billingCycle}
                                setBillingCycle={this.setBillingCycle}
                                nextStep={this.nextStep}
                                prevStep={this.prevStep}
                            />
                        }
                        {this.state.step === 3 &&
                            <ProcessPayment
                                paymentType={this.state.paymentType}
                                billingCycle={this.state.billingCycle}
                                email={this.state.email}
                                prevStep={this.prevStep}
                            />
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default PaymentForm;